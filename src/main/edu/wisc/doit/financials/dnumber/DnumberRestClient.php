<?php

namespace edu\wisc\doit\financials\dnumber;

use Unirest\Exception;
use Unirest\Request;
use Unirest\Response;

/**
 * DnumberRestClient is a REST client for interacting with the DoIT Number API.
 */
class DnumberRestClient implements DnumberClient
{

    /** Base URL for REST API */
    const BASE_URL = 'https://esb.services.wisc.edu:8243/dnumber/api/v1';

    /** Base URL for DoIT Number API (/dnumbers) */
    const DOIT_NUMBER_BASE_URL = self::BASE_URL . '/dnumbers/';

    /**
     * @param string $username
     * @param string $password
     */
    public function __construct($username, $password)
    {
        // Setup HTTP Basic auth
        Request::auth($username, $password);
    }

    /**
     * {@inheritdoc}
     */
    public function retrieveById($id)
    {
        $response = null;
        $apiResponse = null;
        if (preg_match('/^[d|D]\d{6}$/', $id) == 1) {
            try {
                /** @var Response $apiResponse */
                $apiResponse = Request::get(static::DOIT_NUMBER_BASE_URL . $id);
            } catch (Exception $e) {
                throw new DnumberException($apiResponse->raw_body, $apiResponse->code);
            }
        }

        if ($apiResponse !== null) {
            if ($apiResponse->code == 200) {
                $response = $this->parseResponse($apiResponse);
            } else if ($apiResponse->code != 404) {
                throw new DnumberException($apiResponse->raw_body, $apiResponse->code);
            }
        }

        return $response;
    }

    /**
     * Parse a response from an API call to a {@link DnumberResponse} object.
     *
     * @param $response Response Response from API call
     * @return DnumberResponse
     */
    private function parseResponse($response)
    {
        $body = $response->body;
        return new DnumberResponse(
            $body->id,
            $body->reference,
            new FundingSource(
                $body->fundingSource->campusCode,
                $body->fundingSource->dds,
                $body->fundingSource->fund,
                $body->fundingSource->project,
                $body->fundingSource->program,
                $body->fundingSource->account,
                $body->fundingSource->udds,
                $body->fundingSource->group->displayName
            ),
            new \DateTime($body->startDate),
            new \DateTime($body->endDate),
            \DateTime::createFromFormat('U', $body->lastModified), // UNIX timestamp
            $body->lastModifiedBy,
            $body->active
        );
    }

}
