<?php

namespace edu\wisc\doit\financials\dnumber;

/**
 * MockDnumberClient is a mock implementation of {@link DnumberClient} suitable for development and testing.
 */
class MockDnumberClient implements DnumberClient
{

    /** @var bool */
    private $active;

    /** @var \DateTime */
    private $startDate;

    /** @var \DateTime */
    private $activeEndDate;

    /** @var \DateTime */
    private $inactiveEndDate;

    /** @var \DateTime */
    private $lastModified;

    /**
     * MockDnumberClient constructor. Passing in 'false' will force returned responses to be for an inactive DoIT Number
     * and change the end date accordingly.
     *
     * @param bool $active
     */
    public function __construct($active = true)
    {
        $this->active = $active;
        $this->startDate = new \DateTime("19991231");
        $this->activeEndDate = new \DateTime("20200101");
        $this->inactiveEndDate = new \DateTime("20001231");
        $this->lastModified = new \DateTime("19991231");
    }

    /**
     * {@inheritdoc}
     */
    public function retrieveById($id)
    {
        $endDate = $this->active ? $this->activeEndDate : $this->inactiveEndDate;
        return new DnumberResponse(
            $id,
            "ref",
            new FundingSource(
                "A", "06", "0123", "0123456", "program", "account", "A060123", "Test Group"
            ),
            $this->startDate,
            $endDate,
            $this->lastModified,
            "bbadger@wisc.edu",
            $this->active
        );
    }
}
