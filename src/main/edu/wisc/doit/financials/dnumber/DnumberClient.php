<?php

namespace edu\wisc\doit\financials\dnumber;

/**
 * DnumberClient is an interface which defines the supported DoIT Number API endpoints.
 */
interface DnumberClient
{

    /**
     * Retrieve a DoIT Number by its ID.
     *
     * @param string $id
     * @return DnumberResponse|null
     */
    public function retrieveById($id);

}
