<?php

use edu\wisc\doit\financials\dnumber\DnumberClient;
use edu\wisc\doit\financials\dnumber\DnumberRestClient;
use edu\wisc\doit\financials\dnumber\DnumberResponse;

class DnumberRestClientIT extends \PHPUnit_Framework_TestCase
{

    /** @var DnumberClient */
    private $dnumberClient;

    /** @var array integration test data */
    private static $itData;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        $itDataPath = __DIR__ . '/../../../../../resources/it-configuration.ini';
        static::$itData = parse_ini_file($itDataPath);
        if (static::$itData == false) {
            static::fail("Unable to read configuration data from $itDataPath");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->dnumberClient = new DnumberRestClient(
            static::$itData['dnumber.username'],
            static::$itData['dnumber.password']
        );
    }

    /**
     * @test
     */
    public function retrieveById_withValidId()
    {
        /** @var DnumberResponse $response */
        $response = $this->dnumberClient->retrieveById('D000222');
        self::assertNotNull($response);
        self::assertEquals('D000222', $response->getId());
    }

    /**
     * @test
     */
    public function retrieveById_withInvalidId()
    {
        $response = $this->dnumberClient->retrieveById('BACON');
        self::assertNull($response);
    }

}