# dnumber-client-php

A PHP client for interacting with the [DoIT Number](https://git.doit.wisc.edu/adi-ia/dnumber) API.

## Developer Requirements

* PHP >= 5.3
* Phing

## Installing with Composer

To include `dnumber-client-php` in your project, add the following to your project's `composer.json` file:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "git@git.doit.wisc.edu/adi-ia/dnumber-client-php.git"
    }
  ]
}
```

## Example Usage

```php
<?php
use edu\wisc\doit\financials\dnumber\DnumberRestClient;

$client = new DnumberRestClient($username, $password);
$response = $client->retrieveById($id);
if ($response != null) {
    $active = $response->isActive();
    ...
}
```

### Mock Client

A mock client is provided at `edu\wisc\doit\financials\dnumber\MockDnumberClient`. By default, the mock client always
returns a response for an active DoIT Number.

## Testing

Unit tests can be run with:

```
phing test
```

Integration tests can be run with:

```
phing integration-test
```

Integration tests require credentials for the DoIT Number API be in the file
`src/test/resources/it-configuration.ini`. A sample `it-configuration.ini` file is located at
`src/test/resources/it-configuration-SAMPLE.ini`.

## Releasing

You must have write access to the upstream repository to release the client. Assuming you already have an `upstream` remote set:

1. Determine the version number you want to release. This is generally the version already in `composer.json`.
2. `git pull upstream master` to ensure you have the latest changes.
3. Verify the version number is correct in `composer.json`.
4. `composer update`
5. Commit and push any changes to `composer.json` and `composer.lock` to the upstream project (no PR required).
6. `phing release -Drelease.version=x.x.x`, where `x.x.x` is the version to release
7. Increment the version number in `composer.json` by a patch version.
8. Commit and push changes to the upstream repository (no PR required).
